# README #

This is a POC for sending/receiving events to/from Kafka. 

This POC is built with Kafka running on Docker images. You may have to update the IP to work with your own environment. 

## Setup Docker ##

download the YML file from here:
https://bitbucket.org/scpprd/loyalty/src/59ced15133c670aa853b8f008efa0dbd53a53fd9/docs/?at=develop 

I renamed it locally to **all.yml**. You need to update the **KAFKA_ADVERTISED_HOST_NAME** to match your own local IP. 

*Note*
I am assuming in Mac, it is probably OK to use "localhost"; but in Windows box, my Docker is running on top of a virtual box, so I have to specify a different IP to it. 

Use this command to bring up all images:

    docker-compose -f all.yml up 

Leave the docker terminal running. 

# DB setup #

connect to your cassandra db using cqlsh. 

create a table using this script 

```
#!sql

CREATE TABLE IF NOT EXISTS customer1 (
 customer_id int,
 first_name text,
 last_name text,
 PRIMARY KEY (customer_id)); 
 
```

then add some records to it. 

# Run it #

Just use "sbt run", you will see 3 apps. 
1) consumer
2) producer
3) service launcher: start a rest service

# Done #

* A rest service at http://127.0.0.1:9001/v1/customer/[ID]
* the second one will give you a customer from cassandra db. 

# TODO #

* unit test
* add actor to distribute messages
* post customer
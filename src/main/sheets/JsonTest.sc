import com.starbucks.kafkaPoc.model.MessageModel
import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._
import io.circe.generic.semiauto._

val message = MessageModel(123, "Hello World")

def jsonParser[A: Encoder] (obj: A) : String = {
  obj.asJson.noSpaces
}

println (jsonParser(message))

val json = message.asJson.noSpaces

val obj = decode[MessageModel](json)
class BaseMessage(val content:String)
case class MessageA(override val content: String ) extends BaseMessage(content)
class MessageHandler[T<:BaseMessage](message:T) {
  println(message.content)
}
val msgA = MessageA("hello world")
new MessageHandler(msgA)
// new MessageHandler("Another message")

val value:String = "x"
def isEmpty(x: String) = x == null || Option(x.trim).forall(_.isEmpty)
isEmpty(value)

val o = Option(value)
o match {
  case Some(x) =>
    if (x.trim.length == 0)
      println("x is empty")
    else
      println(x)
  case None =>
    println("x is empty")
}
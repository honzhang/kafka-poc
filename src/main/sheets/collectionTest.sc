trait ParentConfig {
  val prefix = "test"
  val keys = getKeys

  // load each key from config file
  keys.foreach(key => println(prefix + key))
  def getKeys : Seq[Int] = {
    Seq(1,2,3)
  }
}

object ChildConfig extends ParentConfig {
  override val prefix = "test123"
  override def getKeys = {
     super.getKeys :+ 4
  }
}

ChildConfig
package com.starbucks.rest.services

import com.starbucks.events.CustomerModel.{Customer, CustomerInsert}
import com.starbucks.rest.repo._

import scala.concurrent.{ExecutionContext, Future}

/**
  * Project: kafka-poc
  * Author: honzhang on 10/26/2016.
  */
class CustomerService(val repo: CustomerRepository)(implicit ec:ExecutionContext) {

  def handleCreateCustomerReceived(model: CustomerInsert): Future[Int] = {
    repo.insertCustomer(model)
  }

  def handleGetter(key: Option[Int]): Future[Option[Customer]] = {
    repo.findCustomerById(key.get)
  }
}

package com.starbucks.rest.services

import com.starbucks.events.PointPostEvent
import com.starbucks.kafkaPoc.clients.Producer
import com.starbucks.kafkaPoc.model.MessageModel
import kafka.network.InvalidRequestException
import io.circe.generic.auto._

import scala.concurrent.Future
import scala.util.Random

/**
  * Project: kafka-poc
  * Author: honzhang on 10/21/2016.
  */
class PointService {
  def getter() : String =
    {
      "Point Service getter"
    }

  def getByIdAndPostEvent(id:String): String = {
    val p =  new Producer[PointPostEvent]("test")
    p.sendModel(PointPostEvent(id, Random.nextInt(10000)))
    "Success"
  }

  def handlePost(pointPostEvent: PointPostEvent) : Future[Option[PointPostEvent]] = {
    val p =  new Producer[PointPostEvent]("test")
    p.sendModel(pointPostEvent)
    Future.successful(Option(pointPostEvent))
  }
}

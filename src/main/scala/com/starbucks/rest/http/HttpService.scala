package com.starbucks.rest.http

import akka.http.scaladsl.server.Directives._
import com.starbucks.rest.http.routes.{CustomerServiceRoute, PointServiceRoute}
import com.starbucks.rest.services.{CustomerService, PointService}

import scala.concurrent.ExecutionContext

/**
  * Project: kafka-poc
  * Author: honzhang on 10/21/2016.
  */
class HttpService(customerService: CustomerService)
                 (implicit exexcutionContext: ExecutionContext) extends CorsSupport {
  //val pointRoute = new PointServiceRoute(pointService)
  val customerRoute = new CustomerServiceRoute(customerService)

  val routes =
    pathPrefix("v1") {
      corsHandler(
        customerRoute.route
      )
    }
}

package com.starbucks.rest.http.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.starbucks.events.CustomerModel.CustomerInsert
import com.starbucks.rest.directives.SecurityDirective
import com.starbucks.rest.services.CustomerService
import de.heikoseeberger.akkahttpcirce.CirceSupport
import io.circe.syntax._

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

/**
  * Project: kafka-poc
  * Author: honzhang on 10/26/2016.
  */
class CustomerServiceRoute(val service: CustomerService)
                          (implicit ec: ExecutionContext)
  extends CirceSupport with SecurityDirective {

  import service._

  val route = pathPrefix("customer") {
    defaultPathRoute ~
    addCustomerRoute ~
      getByKeyRoute
  }

  def defaultPathRoute: Route =  pathEndOrSingleSlash {
      get { // if there is nothing, we should reject it always
          reject()
      }
    }

  def addCustomerRoute: Route = pathEndOrSingleSlash {
    post {
      entity(as[CustomerInsert]) {
        evt => {
          complete(handleCreateCustomerReceived(evt).map(_.asJson))
        }
      }
    }
  }

  def getByKeyRoute: Route = (get & path(IntNumber)) {
    id => onComplete(handleGetter(Option(id))
    ) {
      case Success(option) => option match {
        case Some(customer) => complete(customer)
        case None => complete(StatusCodes.NotFound, "Not found user")
      }
      case Failure(ex) => complete("Error:" + ex.getMessage)
    }
  }
}

package com.starbucks.rest.http.routes

import akka.http.scaladsl.server.Directives._
import com.starbucks.events.PointPostEvent
import com.starbucks.rest.directives.SecurityDirective
import com.starbucks.rest.services.PointService
import de.heikoseeberger.akkahttpcirce.CirceSupport
import scala.concurrent.ExecutionContext
import io.circe.generic.auto._
import io.circe.syntax._

/**
  * Project: kafka-poc
  * Author: honzhang on 10/21/2016.
  */
class PointServiceRoute(val service: PointService)
                       (implicit executionContext: ExecutionContext)
  extends CirceSupport with SecurityDirective {

  import service._

  val route = pathPrefix("point") {
    pathEndOrSingleSlash {
      get {
        complete(getter())
      } ~
      post {
        entity(as [PointPostEvent]) {
          evt => {
            complete(handlePost(evt).map(_.asJson))
          }
        }
      }

    } ~
      (get & path(Segment)) {
        id => complete(
          getByIdAndPostEvent(id)
        )
      }
  }
}

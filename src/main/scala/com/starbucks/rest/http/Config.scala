package com.starbucks.rest.http

import java.net.InetAddress

import com.typesafe.config.ConfigFactory
import com.websudos.phantom.connectors.{ContactPoint, ContactPoints}

import scala.collection.JavaConversions._

/**
  * Project: kafka-poc
  * Author: honzhang on 10/21/2016.
  */
trait Config {
  protected val config = ConfigFactory.load()
  protected val httpConfig = config.getConfig("http")

  val httpHost = httpConfig.getString("host")
  val httpPort = httpConfig.getInt("port")
}

object DatabaseConfig extends Config {
  protected val databaseConfig = config.getConfig("database")
  val hosts = databaseConfig.getStringList("cassandra.host")
  val inets = hosts.map(InetAddress.getByName)

  val keyspace: String = databaseConfig.getString("cassandra.keyspace")
  lazy val connector = ContactPoints(hosts).withClusterBuilder(
    _.withCredentials(
      databaseConfig.getString("cassandra.username"),
      databaseConfig.getString("cassandra.password")
    )
  ).keySpace(keyspace)

  /**
    * Create an embedded connector, used for testing purposes
    */
  lazy val testConnector = ContactPoint.embedded.noHeartbeat().keySpace("customer_test_db")
}

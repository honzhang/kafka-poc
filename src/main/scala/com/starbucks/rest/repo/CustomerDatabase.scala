package com.starbucks.rest.repo

import com.websudos.phantom.db.DatabaseImpl
import com.websudos.phantom.dsl.KeySpaceDef
import scala.concurrent.duration._
import com.starbucks.rest.http.DatabaseConfig._

import scala.concurrent.Await

/**
  * Project: kafka-poc
  * Author: honzhang on 11/2/2016.
  */
class CustomerDatabase(override val connector: KeySpaceDef) extends DatabaseImpl(connector) {
  object customerModel extends ConcreteCustomerModel with connector.Connector
}

object ProductionDb extends CustomerDatabase(connector) {
  Await.ready(customerModel.create.ifNotExists().future(), 3.seconds)
}

trait ProductionDatabaseProvider {
  def database: CustomerDatabase
}

// this db is pointing to a test connector, which will create an embedded cassandra in memory
// so we can simply use this for testing
object EmbeddedDb extends CustomerDatabase(testConnector)

trait EmbeddedDatabaseProvider {
  def database: CustomerDatabase
}



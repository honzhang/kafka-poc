package com.starbucks.rest.repo

import com.datastax.driver.core.{ConsistencyLevel, Row}
import com.websudos.phantom.CassandraTable
import com.websudos.phantom.dsl._
import com.websudos.phantom.keys.PartitionKey

import scala.concurrent.Future
import scala.util.Random

/**
  * Create the Cassandra representation of the Customer table
  */
class CustomerModel extends CassandraTable[ConcreteCustomerModel, CustomerEntity] {

  override def tableName: String = "Customer"

  object id extends IntColumn(this) with PartitionKey[Int] {
    override lazy val name = "customer_id"
  }

  object first_name extends OptionalStringColumn(this)

  object last_name extends OptionalStringColumn(this)

  override def fromRow(r: Row): CustomerEntity = CustomerEntity(id(r), first_name(r), last_name(r))
}

abstract class ConcreteCustomerModel extends CustomerModel with RootConnector {
  def getById(id: Int): Future[Option[CustomerEntity]] = {
    select
      .where(_.id eqs id)
      .consistencyLevel_=(ConsistencyLevel.ONE)
      .one()
  }

  def addCustomer(customer: CustomerEntity): Future[Int] = {
    val id = Random.nextInt(Int.MaxValue)
    insert
      .value(_.id, id)
      .value(_.first_name, customer.first_name)
      .value(_.last_name, customer.last_name)
      .consistencyLevel_=(ConsistencyLevel.ONE)
      .future()
      .map(_ => id)
  }
}

// this is the Scala representation of the Customer
case class CustomerEntity(customer_id: Int, first_name: Option[String], last_name: Option[String])

object CustomerEntity {
  def apply(first_name: Option[String], last_name: Option[String]): CustomerEntity = new CustomerEntity(-1, first_name, last_name)
  def apply(first_name: String, last_name: String): CustomerEntity = new CustomerEntity(-1, Option(first_name), Option(last_name))
}


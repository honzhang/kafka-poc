package com.starbucks.rest.repo

import com.datastax.driver.core.{ConsistencyLevel, Row}
import com.starbucks.events.CustomerModel.{Customer, CustomerInsert}
import com.websudos.phantom.dsl.{CassandraTable, IntColumn, OptionalStringColumn, RootConnector}
import com.websudos.phantom.keys.PartitionKey
import io.getquill.{CassandraAsyncContext, SnakeCase}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Random

trait CustomerRepository {
  def findCustomerById(id: Int): Future[Option[Customer]]

  def insertCustomer(customer: CustomerInsert): Future[Int]
}

// this Quill implementation is not working anymore, do NOT look at it!
case class QuillCustomerRepository(db: CassandraAsyncContext[SnakeCase])
                                  (implicit ec: ExecutionContext)
  extends CustomerRepository {
  // val db  = new CassandraAsyncContext[SnakeCase]("test")
  import db._

  case class customer1(customer_id: Int, first_name: Option[String], last_name: Option[String])

  def findCustomerById(id: Int): Future[Option[Customer]] = {
    val result = db.run(quote {
      (customer_id: Int) =>
        query[customer1].filter(_.customer_id == customer_id)
    }(lift(id)))
    result.map(_.headOption.map(t =>
      Customer(id = t.customer_id,
        firstName = t.first_name.getOrElse(""),
        lastName = t.last_name.getOrElse(""))
    ))
  }

  def insertCustomer(customer: CustomerInsert): Future[Int] = {
    val id = Random.nextInt(Int.MaxValue) // don't use this for real world app
    val result: Future[Unit] = db.run(
      quote {
        query[customer1].insert(lift(
          customer1(
            customer_id = id,
            first_name = Option(customer.firstName),
            last_name = Option(customer.lastName)
          )
        ))
      })

    result.map(_ => id)
  }
}

object QuillCustomerRepository {
  def apply()(implicit ec: ExecutionContext): CustomerRepository = {
    val db = new CassandraAsyncContext[SnakeCase]("test")
    new QuillCustomerRepository(db)
  }
}


case class PhantomCustomerRepository(override val database:CustomerDatabase) (implicit ec: ExecutionContext)
    extends CustomerRepository
    with ProductionDatabaseProvider {
  override def findCustomerById(id: Int): Future[Option[Customer]] = database.customerModel.getById(id).map {
    case Some(customer) => Some(Customer(
      id = customer.customer_id,
      firstName = customer.first_name.getOrElse(""),
      lastName = customer.last_name.getOrElse("")))
    case _ => None
  }

  override def insertCustomer(customer: CustomerInsert): Future[Int] = database.customerModel.addCustomer(
    CustomerEntity(first_name =customer.firstName, last_name = customer.lastName)
  )

}

package com.starbucks.rest

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.starbucks.rest.http.{Config, HttpService}
import com.starbucks.rest.repo.{CustomerRepository, PhantomCustomerRepository, ProductionDb, QuillCustomerRepository}
import com.starbucks.rest.services.{CustomerService, PointService}
import io.getquill.{CassandraAsyncContext, SnakeCase}

import scala.concurrent.ExecutionContext
import scala.io.StdIn

/**
  * Project: kafka-poc
  * Author: honzhang on 10/21/2016.
  */
object ServiceLauncher extends App with Config  {

  implicit val actorSystem = ActorSystem()
  implicit val executor: ExecutionContext = actorSystem.dispatcher
  implicit val log: LoggingAdapter = Logging(actorSystem, getClass)
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  // create a customer repository using a production database
  val repository: CustomerRepository = PhantomCustomerRepository(ProductionDb)

//  val pointService = new PointService

  val customerService=  new CustomerService(repository)
  val httpService = new HttpService(customerService)

  val bindingFuture = Http().bindAndHandle(httpService.routes, httpHost, httpPort)
  println(s"server is starting at $httpHost:$httpPort\nPress Return to stop...")
  StdIn.readLine()
  bindingFuture.onComplete(_=>actorSystem.terminate())
}

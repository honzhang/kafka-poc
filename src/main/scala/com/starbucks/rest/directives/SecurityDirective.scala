package com.starbucks.rest.directives

/**
  * Project: kafka-poc
  * Author: honzhang on 10/21/2016.
  */
trait SecurityDirective {
  def authenticate = {
    true
  }
}

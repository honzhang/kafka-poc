package com.starbucks.kafkaPoc.exceptions

/**
  * Project: kafka-poc
  * Author: honzhang on 10/20/2016.
  */
case class MissingConfigException(message: String) extends Exception {

}

package com.starbucks.kafkaPoc

import java.util
import java.util.Properties

import com.starbucks.events.CustomerModel.Customer
import io.circe.parser._
import org.apache.kafka.clients.consumer.{ConsumerRecords, KafkaConsumer}
import scala.collection.JavaConversions._

/**
  * Project: kafka-poc
  * Author: honzhang on 10/27/2016.
  */
class DbConsumerApp  extends App {
  val topicName = "test"
  val props = new Properties()

  props.put("bootstrap.servers", "192.168.99.100:9092")
  props.put("group.id", "test")
  props.put("enable.auto.commit", "true")
  props.put("auto.commit.interval.ms", "1000")
  props.put("session.timeout.ms", "30000")
  props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
  props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
  val consumer = new KafkaConsumer[String, String](props)

  //Kafka Consumer subscribes list of topics here.
  consumer.subscribe(util.Arrays.asList(topicName))

  //print the topic name
  println("Subscribed to topic " + topicName)
  val i = 0

  var keepAlive = true
  while (keepAlive) {
    val records: ConsumerRecords[String, String] = consumer.poll(100)
    records.iterator.foreach(record => {
      val json = record.value()
      val customerOrError = decode[Customer](json)

      customerOrError match {
        case customer => {
          // TODO: write to cassandra
        }
        case _ => println("error")
      }

      keepAlive = false
    })
  }
}

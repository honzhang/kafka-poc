package com.starbucks.kafkaPoc.clients

import java.util.Properties
import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._
import com.starbucks.kafkaPoc.config.KafkaConfig
import io.circe.Encoder
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

class Producer[A](topic: String) {
  protected val producer = new KafkaProducer[String, String](KafkaConfig("producer"))

  def sendModel(obj: A)(implicit ev:Encoder[A]) = send(obj.asJson.noSpaces)
  def send(message: String) = sendMessage(producer, producerMessage(topic, message))
  def sendStream(stream: Stream[String]) = {
    val iter = stream.iterator
    while(iter.hasNext) {
      send(iter.next())
    }
  }

  def close() = {
    producer.flush()
    producer.close()
  }

  private def producerMessage(topic: String, message: String) : ProducerRecord[String, String] = new ProducerRecord(topic, message)
  private def sendMessage(producer: KafkaProducer[String, String], message: ProducerRecord[String, String]) = producer.send(message)
}

object Producer {
    def apply[T](topic: String, props: Properties) = new Producer[T](topic) {
      override val producer = new KafkaProducer[String, String](props)
    }
}
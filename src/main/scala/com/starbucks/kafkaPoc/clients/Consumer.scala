package com.starbucks.kafkaPoc.clients

import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.KafkaProducer

/**
  * Project: kafka-poc
  * Author: honzhang on 10/19/2016.
  */
case class Consumer[A](topics: List[String]) extends BaseConsumer(topics) {
  override def read(): Iterable[String] = ???
}

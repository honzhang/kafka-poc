package com.starbucks.kafkaPoc.clients

import com.starbucks.kafkaPoc.config.KafkaConfig

/**
  * Project: kafka-poc
  * Author: honzhang on 10/19/2016.
  */
abstract class BaseConsumer(topics: List[String]) {

  protected val kafkaConfig = KafkaConfig
  def read(): Iterable[String]
}
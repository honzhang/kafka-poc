package com.starbucks.kafkaPoc.config

import java.util.Properties

import com.typesafe.config.ConfigFactory

/**
  * Project: kafka-poc
  * Author: honzhang on 10/19/2016.
  */
case class KafkaConfig(prefix:String) extends Properties {
  lazy val typesafeConfig = ConfigFactory.load()

  def getCustomString(key: String) = typesafeConfig.getString(key)
  def getCustomInt(key: String) = typesafeConfig.getInt(key)

  val prefixWithDot = prefix + "."

  val groupId = s"$prefix.group.id"
  val bootstrapServers = s"$prefix.bootstrap.servers"
  val keySerializer = s"$prefix.key.serializer"
  val valueSerializer = s"$prefix.value.serializer"
  val producerType = s"$prefix.producer.type"
  val sessionTimeout = s"$prefix.session.timeout.ms"
  val autoCommitInterval = s"$prefix.auto.commit.interval.ms"
  val enableAutoCommit = s"$prefix.enable.auto.commit"
  val keyDeserializer = s"$prefix.key.deserializer"
  val valueDeserializer = s"$prefix.value.deserializer"

  getConfigKeys map { key =>
    if (typesafeConfig.hasPath(key))
      put(key.replace(prefixWithDot, ""), getConfigValue(key))
  }

  def getConfigKeys: Seq[String] = {
    prefix match {
      case "consumer" => getConsumerKeys
      case "producer" => getProducerKeys
    }
  }

  def getProducerKeys: Seq[String] = {
    Seq(groupId, bootstrapServers, keySerializer, valueSerializer, producerType)
  }

  def getConsumerKeys: Seq[String] = {
    Seq(groupId, bootstrapServers, sessionTimeout, autoCommitInterval, enableAutoCommit, keyDeserializer, valueDeserializer)
  }

  def getConfigValue(key: String): String = key match {
    case `bootstrapServers` => getConfigValueOrElse(key, "localhost:9092")
    case `producerType` => getConfigValueOrElse (key, "async")
    case `keySerializer` => getConfigValueOrElse(key, "org.apache.kafka.common.serialization.StringSerializer")
    case `valueSerializer` => getConfigValueOrElse(key, "org.apache.kafka.common.serialization.StringSerializer")

    case `sessionTimeout` => getConfigValueOrElse(key, "30000")
    case `autoCommitInterval` => getConfigValueOrElse(key, "1000")
    case `enableAutoCommit` => getConfigValueOrElse(key, "true")
    case `keyDeserializer` => getConfigValueOrElse(key, "org.apache.kafka.common.serialization.StringDeserializer")
    case `valueDeserializer` => getConfigValueOrElse(key, "org.apache.kafka.common.serialization.StringDeserializer")
  }

  def getConfigValueOrElse(key:String, defaultValue: String) :String = {
    Option(typesafeConfig.getString(key))
      .flatMap(v => if (v.isEmpty) None else Some(v))
      .map(v=>v)
      .getOrElse(defaultValue)
  }
}


package com.starbucks.kafkaPoc.model

/**
  * Project: kafka-scala-poc
  * Author: honzhang on 10/18/2016.
  */
case class MessageModel (time: Long, content: String) {

}

package com.starbucks.kafkaPoc

import java.util.Date

import com.starbucks.kafkaPoc.clients.Producer
import com.starbucks.kafkaPoc.model.MessageModel
import io.circe.generic.auto._
import io.circe.syntax._
import org.apache.kafka.clients.producer.ProducerRecord

/**
  * Project: Default (Template) Project
  * Author: honzhang on 10/19/2016.
  */
object ProducerApp extends App {

  val topic = "test"

  val runtime = new Date().getTime
  val msg = MessageModel(runtime, "hello world")

  // the following should generate the same result
  val messageProducer = new Producer[MessageModel](topic)
  for (i <- 1 to 10) {
    val m = MessageModel(i, s"Hello from the other side $i")
    messageProducer.sendModel(msg)
  }

  messageProducer.close
}

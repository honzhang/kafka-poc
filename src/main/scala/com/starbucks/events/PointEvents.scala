package com.starbucks.events

sealed class AccountEvent(val customerId: String) {}

case class PointGetEvent(override val customerId:String) extends AccountEvent(customerId) {}
case class PointPostEvent(override val customerId: String, points: Int) extends AccountEvent(customerId) {}

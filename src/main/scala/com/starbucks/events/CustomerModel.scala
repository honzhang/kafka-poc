package com.starbucks.events

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._

sealed trait CustomerModel

object CustomerModel {

  case class Customer(
                     id:Int,
                     firstName: String,
                     lastName: String
                     ) extends CustomerModel

  object Customer {
      implicit val decodeCreated: Decoder[Customer] = deriveDecoder[Customer]
      implicit val encodeCreated: Encoder[Customer] = deriveEncoder[Customer]
  }

  case class CustomerInsert(
                           firstName:String,
                           lastName:String
                            ) extends CustomerModel
  object CustomerInsert {
    implicit val decodeCreated: Decoder[CustomerInsert] = deriveDecoder[CustomerInsert]
    implicit val encodeCreated: Encoder[CustomerInsert] = deriveEncoder[CustomerInsert]
  }

}



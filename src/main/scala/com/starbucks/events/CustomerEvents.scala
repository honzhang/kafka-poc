package com.starbucks.events

import io.circe._
import io.circe.generic.semiauto._

sealed trait CustomerEvent

object CustomerEvent {
  case class Created(val customerId: Int) extends CustomerEvent
  object Created {
    implicit val decodeCreated: Decoder[Created] = deriveDecoder[Created]
    implicit val encodeCreated: Encoder[Created] = deriveEncoder[Created]
  }
}

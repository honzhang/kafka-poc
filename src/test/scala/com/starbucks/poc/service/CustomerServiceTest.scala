package com.starbucks.poc.service

import org.scalatest.concurrent.ScalaFutures
import io.circe.generic.auto._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{HttpEntity, MediaTypes}

/**
  * Project: kafka-poc
  * Author: honzhang on 10/25/2016.
  */
class CustomerServiceTest extends BaseServiceTest with ScalaFutures{
  import pointService._

  // this is a nice way to provide a test context for each test case
  trait Context {
    val route = httpService.pointRoute.route
    val getter_constant = "Reject"
  }

  "Customer Service Default getter" should {
    "reject" in new Context {
      Get("/point") ~> route ~> check {
        println (responseAs[String])
        responseAs[String] should be(getter_constant)
      }
    }

  }
}

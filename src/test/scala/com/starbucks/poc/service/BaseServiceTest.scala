package com.starbucks.poc.service

import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.starbucks.rest.http.HttpService
import com.starbucks.rest.services.{CustomerService, PointService}
import de.heikoseeberger.akkahttpcirce.CirceSupport
import org.scalatest.{Matchers, WordSpec}

/**
  * Project: kafka-poc
  * Author: honzhang on 10/25/2016.
  */
trait BaseServiceTest extends WordSpec with Matchers with ScalatestRouteTest with CirceSupport {
  val pointService = new PointService()
  val customerService = CustomerService()
  val httpService = new HttpService(pointService, customerService)
}
